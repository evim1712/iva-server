const crypto = require('crypto');

const hashSize = 32;
const saltSize = 16;
const hashAlgorithm = 'sha512';
const iterations = 1000;

exports.createHash = password => new Promise((resolve, reject) => {
  crypto.randomBytes(saltSize, (err, salt) => {
    if (err) {
      reject(new Error('Salt generation unsuccessful'));
    }
    // hash készítése
    crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (cryptErr, hash) => {
      if (cryptErr) {
        reject(new Error('Hashing unsuccessful'));
      } else {
        const hashWithSalt = Buffer.concat([hash, salt]).toString('hex');
        resolve(hashWithSalt);
      }
    });
  });
});

exports.checkHash = (password, hashWithSalt) => {
  const expectedHash = hashWithSalt.substring(0, hashSize * 2),
    salt = Buffer.from(hashWithSalt.substring(hashSize * 2), 'hex');
  return new Promise((resolve, reject) => {
    crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (err, binaryHash) => {
      if (err) {
        reject(new Error('Hashing unsuccessful'));
      } else {
        const actualHash = binaryHash.toString('hex');

        if (expectedHash === actualHash) {
          resolve(true);
        } else {
          reject(new Error('The name or password is incorrect'));
        }
      }
    });
  });
};
