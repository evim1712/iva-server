const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const userRoutes = require("./api/routes/rUser");
const internshipRoutes = require('./api/routes/rInternship');
const session = require('express-session');

const app = express();

app.use(cors({
  origin: "*",
  credentials: true,
}));

app.use(session({
  secret: '142e6ecf42884f03',
  resave: false,
  saveUninitialized: true,
}));

app.use(bodyParser.json());

app.use('/users', userRoutes);
app.use('/internships', internshipRoutes);

app.use((req, res) => {
  res.status(404).send(`Adress (${req.url}) not found`);
});

const PORT = 8080;

app.listen(PORT, () => { console.log(`server listening at ${PORT}..`); });

module.exports = app;
