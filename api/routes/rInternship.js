// Router file
const express = require('express');
const dbInternship = require('../db/internship');

const router = express.Router();

router.get("/", (req, res) => {
  res.send(dbInternship.getInternships());
});

router.post("/", (req, res) => {
  const { role, company, location, duration, startDate,
    payed, salary, education, description } = req.body;

  dbInternship.insertInternship({
    role, company, location, duration, startDate,
    payed, salary, education, description
  }).then(result => res.send(result))
    .catch(error => res.status(500).send(error.message));
});

module.exports = router;
