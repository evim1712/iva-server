// Router file
const express = require('express');
const dbUser = require('../db/user');
const passwordHash = require('../../utils/passwordHash');

const router = express.Router();

router.post('/login', (req, res) => {
  const username = String(req.body.username);
  const password = String(req.body.password);

  const user = dbUser.findUserByUsername(username);

  passwordHash.checkHash(password, user.password)
    .then(() => {
      req.session.username = username;
      req.session.role = user.role;
      res.send(`Hello ${username}, you are an ${user.role}`);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(404);
    });
});

router.post('/register', (req, res) => {
  const { firstname, lastname, username, email, password, role } = req.body;

  if (!firstname || !lastname || !username || !email || !password || !role) {
    res.status(400).send('Parameter missing or invalid');
    return;
  }

  passwordHash.createHash(password)
    .then((hashedPassword) => {
      dbUser.createUser({
        firstname,
        lastname,
        username,
        email,
        hashedPassword,
        role
      }).then(() => {
        res.sendStatus(201)
      });
    }).catch(err => res.status(500).send(err.message));
});

module.exports = router;
