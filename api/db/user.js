const loki = require('lokijs');
const db = new loki('sandbox.db');

// Add a collection to the database
const users = db.addCollection('users');
const nextId = 5;

// USERNAME, PASSWORD:
// admin, admin123456
// university, uni123456
// student, stud123456
// company, com123456
users.insert({
  id: 1,
  firstname: 'admin',
  lastname: 'admin',
  username: 'admin',
  email: 'admin@yahoo.com',
  password: '4a2f33c81815c3150984c3cfa653d641404542608e66453157122a54b31b0f066bb85b0dff870184af5cb6dd424f4c12',
  role: 'admin'
});

users.insert({
  id: 2,
  firstname: 'Babes Bolyai',
  lastname: 'University',
  username: 'university',
  email: 'uni@yahoo.com',
  password: '6f18bc451a4f1d159ad82690250527e459db7f7504f710c2633b355e109fd88f66e22deedc5564d8aaded7b24d39c037',
  role: 'uni'
});

users.insert({
  id: 3,
  firstname: 'Student',
  lastname: 'One',
  username: 'student',
  email: 'stud@yahoo.com',
  password: '286f3e8dc64b53739ade368ddbe769fbd9eeeafe14103a6dc2efedf09500f35cd3ff9e82548c88ed8e08031b5004333e',
  role: 'student'
});

users.insert({
  id: 4,
  firstname: 'Company',
  lastname: 'One',
  username: 'company',
  email: 'company@yahoo.com',
  password: '47b3e00bbab63dd4b59baa9d171e1b2ed2c1855979cd46a824ede098178634aa69271dedbddcc8d3bcbe01a191c0a460',
  role: 'company'
});

const findUserByUsername = (username) => users.findOne({ 'username': username });

const createUser = (user) => new Promise((resolve, reject) => {
  const before = users.count();
  users.insert({
    id: nextId,
    firstname: user.firstname,
    lastname: user.lastname,
    username: user.username,
    email: user.email,
    password: user.hashedPassword,
    role: user.role
  });
  if (before === users.count()) {
    reject({ message: "Something went wrong" });
  } else {
    resolve();
    nextId = nextId + 1;
  }
});

module.exports = {
  findUserByUsername,
  createUser
}