const loki = require('lokijs');
const db = new loki('sandbox.db');

const internships = db.addCollection('internships');
let nextId = 2;

internships.insert({
  id: 1,
  role: "programmer",
  company: "NTTData",
  location: "Cluj",
  duration: 6,
  startDate: 1610409638,
  payed: "true",
  salary: "500",
  education: "bachelor's degree in computer science",
  description: ""
});

const getInternships = () => {
  const list = internships.findObjects();
  list.forEach(internship => { delete internship.meta; delete internship.$loki });
  return list;
};

const insertInternship = (internship) => new Promise((resolve, reject) => {
  const before = internships.count();

  internships.insert({
    id: nextId,
    role: internship.role,
    company: internship.company,
    location: internship.location,
    duration: internship.duration,
    startDate: internship.startDate,
    payed: internship.payed,
    salary: internship.salary,
    education: internship.education,
    description: internship.description
  });

  if (before === internships.count()) {
    reject({ message: "Something went wrong" });
  } else {
    resolve(internships.find({ "id": nextId }))
    nextId = nextId + 1;
  }
});

const findInternshipById = (id) => internships.findOne({ "id": id });

module.exports = {
  getInternships,
  insertInternship,
  findInternshipById
}